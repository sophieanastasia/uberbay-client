import { GET_HAPPENINGS, POST_HAPPENINGS_SUCCESS } from '../actions/happenings'

const reducer = (state = [], action = {}) => {
  switch(action.type) {
    case GET_HAPPENINGS:
      return {
        happenings: (state.happenings || []).concat(action.payload.happenings),
        hasMore: action.payload.hasMore
      }
    default:
      return state 
  }
}

export default reducer