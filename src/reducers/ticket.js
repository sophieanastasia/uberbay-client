import { GET_CURRENT_TICKET, EDIT_TICKET } from '../actions/tickets'
import { GET_COMMENTS } from '../actions/tickets'

const reducer = (state = [], action = {}) => {
  switch(action.type) {
    case GET_CURRENT_TICKET:
      return {
        ...action.payload
      }
    case EDIT_TICKET:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state 
  
    case GET_COMMENTS: 
      return {
        ...state, 
        comments: [...action.payload.comments]
      }
  }
}

export default reducer