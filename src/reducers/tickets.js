import { GET_TICKETS, EDIT_TICKET } from '../actions/tickets'

const reducer = (state = [], action = {}) => {
  switch(action.type) {
    case GET_TICKETS:
      return {
        tickets: action.payload.tickets,
        hasMore: action.payload.hasMore
      }
    case EDIT_TICKET:
      return {
        ...state,
        tickets: (state.tickets || [])
          .map(ticket => {
            if(ticket.id !== action.payload.id) return ticket

            return {
              ...ticket,
              ...action.payload
            }
          })
      }
    default:
      return state 
  }
}

export default reducer