import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import NavBar from './components/layout/NavBar'
import HappeningsListContainer from './components/happenings/HappeningsListContainer'
import TicketsListContainer from './components/tickets/TicketsListContainer'
import TicketsDetailsContainer from './components/tickets/TicketsDetailsContainer'
import LoginFormContainer from './components/login/LoginFormContainer'
import SignupFormContainer from './components/signup/SignupFormContainer'
import AddTicketFormContainer from './components/tickets/AddTicketFormContainer'
import AddHappeningFormContainer from './components/happenings/AddHappeningFormContainer'
import LogoutPage from './components/logout/LogoutPage'

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <NavBar />
        <Route exact path="/" component={HappeningsListContainer}/>
        <Route exact path="/happenings/:id/tickets" component={TicketsListContainer}/>
        <Route exact path="/tickets/:id" component={TicketsDetailsContainer}/>
        <Route exact path="/login" component={LoginFormContainer}/>
        <Route exact path="/logout" component={LogoutPage}/>
        <Route exact path="/signup" component={SignupFormContainer}/>
        <Route exact path="/addticket" component={AddTicketFormContainer}/>
        <Route exact path="/addhappening" component={AddHappeningFormContainer}/>
      </div>
    );
  }
}

export default App;
