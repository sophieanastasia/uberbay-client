import React, {PureComponent} from 'react'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'

class SignupForm extends PureComponent {
	state = {}

	handleSubmit = (e) => {
		e.preventDefault()
		this.props.onSubmit(this.state)
	}

	handleChange = (event) => {
    const {name, value} = event.target

    this.setState({
      [name]: value
    })
  }

	render() {
    const { classes } = this.props
		return (
      <Paper>
  			<form className={classes.container} onSubmit={this.handleSubmit}>
          <Typography variant="title" >Welcome!</Typography>
          <TextField 
            type="email" 
            name="email" 
            value={this.state.email || ''} 
            onChange={ this.handleChange } 
            label="Email"
            required
          />

          <TextField 
            type="text" 
            name="firstName" 
            value={this.state.firstName || ''} 
            onChange={ this.handleChange } 
            label="First name"
            required
          />

          <TextField 
            type="text" 
            name="lastName" 
            value={this.state.lastName || ''} 
            onChange={ this.handleChange } 
            label="Last name"
            required
          />

          <TextField 
            type="password" 
            name="password" 
            value={this.state.password || ''} 
            onChange={ this.handleChange } 
            label="Password"
            required
          />

          <TextField 
            type="password" 
            name="confirmPassword" 
            value={this.state.confirmPassword || ''} 
            onChange={ this.handleChange } 
            label="Password"
            required
          />

  				{
  					this.state.password &&
  					this.state.confirmPassword &&
  					this.state.password !== this.state.confirmPassword &&
  					<p style={{color:'red'}}>The passwords do not match!</p>
  				}

  				<Button type="submit">Sign up</Button>
  			</form>
      </Paper>
		)
	}
}

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    flexDirection: 'column',
    height: '50vh',
    justifyContent: 'center'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
})

export default withStyles(styles)(SignupForm)