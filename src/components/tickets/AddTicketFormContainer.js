import * as React from 'react'
import { addTicket } from '../../actions/tickets'
import { connect } from 'react-redux'
import AddTicketForm from './AddTicketForm'
import {withRouter} from 'react-router'

class AddTicketFormContainer extends React.PureComponent {
  handleSubmit = (state) => {
    this.props.addTicket(state)
      .then(response => {
        this.props.history.push(`/tickets/${response.id}`)
      })
  }

  render() {
    return (
      <div>
        <AddTicketForm handleSubmit={this.handleSubmit} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { 
    tickets: state.tickets ? state.tickets.tickets : null,
    hasMore: state.tickets ? state.tickets.hasMore : null
  }
}

export default withRouter(
  connect(mapStateToProps, { addTicket })(AddTicketFormContainer)
)