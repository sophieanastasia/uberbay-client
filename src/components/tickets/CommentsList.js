import * as React from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';

export default class CommentsList extends React.PureComponent {
  renderComment = (comment) => {
    const postDate = new Date(comment.postDate)
    return(
      <ListItem key={comment.id}>
        <Typography variant="body2">{comment.comment}</Typography>
        <Typography variant="caption"> - {comment.postDate} - {comment.user.firstName}</Typography>
      </ListItem>
    )
  }

  render() {
    return (
      <div>
        <Typography variant="subheading">Comments</Typography>
        <List>{this.props.comments.map(this.renderComment) }</List>
      </div>
    )
  }
}