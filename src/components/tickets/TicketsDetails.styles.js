export const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 1080,
    alignSelf: 'center',
    marginTop: '70px',
    margin: 'auto'
  },
  list: {
    display: 'flex',
    justify: 'space-evenly',
  },
  listItem: {
    display: 'flex',
    justifyContent: 'space-evenly',
    margin: '1em',
    backgroundColor: '#fff'
  }, 
  riskButton: {
    borderRadius: '3px', 
    padding: '.4em 1em',
    opacity: '.8'
  }
});