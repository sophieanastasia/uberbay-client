import React, {PureComponent} from 'react'
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'

class AddTicketForm extends PureComponent {
	state = {}

	handleSubmit = (e) => {
		e.preventDefault()
		this.props.handleSubmit(this.state)
	}

	handleChange = (event) => {
    const {name, value} = event.target

    this.setState({
      [name]: value
    })
  }

	render() {
    const { classes } = this.props
		return (
      <Paper style={{marginTop: '70px'}} className="addticket-form">
  			<form className={classes.container} onSubmit={this.handleSubmit}>
          <Typography variant="title"> Sell your Ticket</Typography>
          <TextField 
            type="number"
            className={classes.textField}
            name="happening" 
            required
            value={
              this.state.happening || ''
            } 
            onChange={ this.handleChange } 
            label="Happening ID"
          />

          <TextField 
            type="text" 
            className={classes.textField}
            name="description" 
            required
            value={
              this.state.description || ''
            } 
            onChange={ this.handleChange } 
            label="Description"
          />

          <TextField 
            type="number" 
            className={classes.textField}
            name="price" 
            required
            value={
              this.state.price || ''
            } 
            onChange={ this.handleChange } 
            label="Price"
          />

          <TextField 
            type="text" 
            className={classes.textField}
            name="image" 
            value={
              this.state.image || ''
            } 
            onChange={ this.handleChange } 
            label="Photo"
          />
          <Button type="submit">Sell Ticket</Button>
  			</form>
      </Paper>
		)
	}
}

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    flexDirection: 'column',
    height: '50vh',
    justifyContent: 'center'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
})

export default withStyles(styles)(AddTicketForm)