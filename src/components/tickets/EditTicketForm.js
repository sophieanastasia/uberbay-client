import React, {PureComponent} from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'

export default class EditTicketForm extends PureComponent {
	state = {}

	componentDidMount() {
		this.setState({
			description: this.props.description || null,
			price: this.props.price,
			image: this.props.image || null
		})
	}
	handleSubmit = (e) => {
		e.preventDefault()
		this.props.handleSubmit(this.state)
	}

	handleChange = (event) => {
    const {name, value} = event.target

    this.setState({
      [name]: value
    })
  }

	render() {
		return (
      <div className="signup-form">
  			<form onSubmit={this.handleSubmit}>

						<TextField 
							type="text" 
							name="description" 
							value={this.state.description || ''} 
							onChange={this.handleChange} 
							fullWidth
							label="description" 
						/>

						<TextField
							type="number" 
							name="price" 
							value={this.state.price || ''} 
							onChange={ this.handleChange } 
							fullWidth
							label="price"
						/>

						<TextField
							type="text" 
							name="image" 
							value={this.state.image || ''} 
							onChange={ this.handleChange } 
							fullWidth
							label="Ticket Image URL"
						/>

  				<Button type="submit">Update ticket</Button>
  			</form>
      </div>
		)
	}
}