import React, {PureComponent} from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

export default class AddCommentForm extends PureComponent {
	state = {}

	handleSubmit = (e) => {
		e.preventDefault()
		this.props.handleSubmit(this.state)
	}

	handleChange = (event) => {
    const {name, value} = event.target

    this.setState({
      [name]: value
    })
  }

	render() {
		return (
      <div className="comment-form">
  			<form onSubmit={this.handleSubmit}>
            <TextField
              id="comment"
              name="comment"
              label="Add a comment"
              multiline
              rowsMax="4"
              value={this.state.comment || ""}
              onChange={this.handleChange}
              margin="normal"
            />
          <Button type="submit" value="submit">Submit</Button>
        </form>
      </div>
		)
	}
}