import * as React from 'react'
import TicketsList from './TicketsList'
import { getTickets } from '../../actions/tickets'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

class TicketsListContainer extends React.PureComponent {
  componentDidMount() {
    this.props.getTickets(this.props.match.params.id, 0)
  }

  getMoreTickets = () => {
    this.props.getTickets(this.props.match.params.id, this.props.tickets.length)
  }
  render() {
    return (
      <div>
        {this.props.tickets && <TicketsList tickets={this.props.tickets} />}
        {this.props.tickets &&  this.props.tickets.length < 1 && 
          <div>
            <h3>No tickets available</h3>
            <Link to={"/addticket"}>Add a ticket</Link>
          </div>
        }
        {this.props.hasMore && <button size="medium" color="primary" onClick={this.getMoreTickets}>Get more</button>}
      </div>
    )

  }
}

const mapStateToProps = (state) => {
  return { 
    tickets: state.tickets ? state.tickets.tickets : null,
    hasMore: state.tickets ? state.tickets.hasMore : null
  }
}

export default connect(mapStateToProps, { getTickets })(TicketsListContainer)