const colorTicket = (score) => {
  const colors = {
    34: '#53ff35',
    67: '#f88233',
    100: '#f44244'
  }
}

export const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 1080,
    alignSelf: 'center',
    marginTop: '70px',
    margin: 'auto'
  },
  list: {
    display: 'flex',
    justify: 'space-evenly',
  },
  listItem: {
    display: 'flex',
    justifyContent: 'space-evenly',
    margin: '1em',
    backgroundColor: '#fff'
  }, 
  riskButton: {
    borderRadius: '3px', 
    padding: '.4em 1em',
    opacity: '.8'
  }
})