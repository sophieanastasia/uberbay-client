import * as React from 'react'
import TicketsDetails from './TicketsDetails'
import CommentsList from './CommentsList'
import AddCommentForm from './AddCommentForm'
import EditTicketForm from './EditTicketForm'
import { userId } from '../../jwt'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'

import { getCurrentTicket, buyTicket, editTicket, addComment, getComments } from '../../actions/tickets'
import { connect } from 'react-redux'

class TicketsDetailsContainer extends React.PureComponent {
  state = {
    editMode: false
  }

  componentDidMount() {
    this.props.getCurrentTicket(this.props.match.params.id)
  }

  buyTicket = () => {
    this.props.buyTicket(this.props.match.params.id)
  }

  handleCommentSubmit = (comment) => {
    console.log(comment)
    this.props.addComment(this.props.match.params.id, comment)
  }

  toggleEditMode = () => {
    this.setState({
      editMode: !this.state.editMode
    })
  }

  handleEditFormSubmit = (changes) => {
    this.props.editTicket(this.props.match.params.id, changes)
    this.toggleEditMode()
  }

  render() {
    const { ticket, userId, comments } = this.props
    return (
      <Paper style={{margin: '80px 2em', padding: '1em'}}>
        {ticket.id && 
        !this.state.editMode &&
        <TicketsDetails ticket={ticket} buyTicket={this.buyTicket} userId={userId}/>}
        
        {this.props.comments && !this.state.editMode &&
          <CommentsList comments={comments}  />}

        {ticket.seller && 
          ticket.seller.id === userId &&
          !this.state.editMode &&
          <Button onClick={this.toggleEditMode}>Edit Ticket</Button>
        }

        {this.state.editMode && 
          <EditTicketForm 
            description={ticket.description} 
            price={ticket.price} 
            image={ticket.image} 

          handleSubmit={this.handleEditFormSubmit} 
        />}

        {userId && 
        !this.state.editMode &&
        <AddCommentForm handleSubmit={this.handleCommentSubmit} />}
      </Paper>
    )
  } 
}

const mapStateToProps = (state) => {
  return { 
    comments: state.ticket.comments ? state.ticket.comments : null,
    ticket: state.ticket,
    userId: state.currentUser && userId(state.currentUser.jwt)
  }
}

export default connect(mapStateToProps, { 
  getComments, 
  getCurrentTicket, 
  buyTicket,
  editTicket, 
  addComment 
})(TicketsDetailsContainer)