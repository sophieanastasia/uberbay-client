import * as React from 'react'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem'

export default class TicketsDetails extends React.PureComponent {
  render() {
    const { ticket, userId } = this.props
    return (
      <div>
          <Typography variant="title">{ticket.happening.name}</Typography>
          {ticket.image && 
          <img src={ticket.image} alt="ticket" />}
          {!ticket.image && 
          <img src={`../../defaultticket.png`} alt="ticket" />}
          <Typography variant="caption">There is a chance of {ticket.riskScore}% that this ticket is a fraud.</Typography>
          <hr />
          <Typography variant="body2" className="description">Description: {ticket.description}</Typography>
          <List className="ticket-medadata">
            <ListItem>{`Sold by: ${ticket.seller.firstName} ${ticket.seller.lastName}`}</ListItem>
            <ListItem>{`Status: ${ticket.status}`}</ListItem>
            <ListItem>{`Post Date: ${ticket.postDate}`}</ListItem>
            <ListItem className="ticket-location">{`Event location: ${ticket.location}`}</ListItem>
            <ListItem className="ticket-price">€ {ticket.price}</ListItem>
          </List>
          {ticket.status === 'available' && 
          userId &&
          ticket.seller.id !== userId &&
          <Button onClick={this.props.buyTicket}>Buy this ticket</Button>}
      </div>
    )
  }
}