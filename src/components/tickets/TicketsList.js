import * as React from 'react'
import { Link } from 'react-router-dom'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { styles } from './TicketsList.styles'
import { withStyles } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';

class TicketsList extends React.PureComponent {

  colorTicket = (score) => {
    const colors = {
      34: '#53ff35',
      67: '#f88233',
      100: '#f44244'
    }
    return colors[Object.keys(colors).find(color => score <= color )]
  }
  renderTicket = (ticket) => {
    const { classes } = this.props
    return (
      <ListItem button key={ticket.id} className={classes.listItem}>
        <Typography variant="button" 
          className={classes.riskButton} 
          style={{background: `${this.colorTicket(ticket.riskScore)}`, 
            border: '1px solid this.colorTicket(ticket.riskScore) '}}>
              {ticket.riskScore}% risk
        </Typography>
        <Typography variant="caption" >€ {ticket.price}</Typography>
        <Typography variant="caption" >Sold by {ticket.seller.firstName[0].toUpperCase() + ticket.seller.firstName.substring(1)}</Typography>
        <Link to={`/tickets/${ticket.id}`}><Button>Get Info</Button></Link>
      </ListItem>
    )
  }

  render() {
    const { classes } = this.props
    return (
    <List className={classes.root} >
      {(!this.props.tickets || this.props.tickets === null) && <h3>No tickets found</h3>}
      {this.props.tickets && this.props.tickets.map(ticket => this.renderTicket(ticket))}
    </List>)
  }
}

export default withStyles(styles)(TicketsList)