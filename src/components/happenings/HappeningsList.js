import * as React from 'react'
import { styles } from './HappeningsList.styles'
import { Link } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import GridListTile from '@material-ui/core/GridListTile';
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import moment from 'moment'
import { withStyles } from '@material-ui/core/styles';

class HappeningsList extends React.PureComponent {
  renderHappening = (happening) => {
    const { classes } = this.props
    return (
      <GridListTile key={happening.id} className={classes.root} item="true" xs={12} sm={6} lg={4} xl={3}>
        <Card className={classes.card}>
          <CardMedia 
            image={happening.image}
            className={classes.media}
            title={happening.name}
          />
          <CardContent>
            <Typography variant="subheading" >{happening.name}</Typography>
            <Typography variant="body1">
              {`${moment(happening.startDate).format("DD-MM-YYYY")} - ${moment(happening.endDate).format("DD-MM-YYYY")}`}
            </Typography>
          </CardContent>
          <CardActions>
            <Link style={{textDecoration: 'none'}} to={`/happenings/${happening.id}/tickets`}>
              <Button>See Tickets</Button>
            </Link>
          </CardActions>
        </Card> 
      </GridListTile>
    )
  }

  render() {
    const { happenings } = this.props
    return (
      <Grid container col={12} spacing={24} style={{padding: 24}}>
          {happenings ? happenings.map(happening => this.renderHappening(happening)) 
            : "No Tickets available"}
      </Grid>
    )
  }
}

export default withStyles(styles)(HappeningsList)