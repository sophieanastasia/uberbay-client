import * as React from 'react'
import HappeningsList from './HappeningsList'
import { connect } from 'react-redux'
import { getHappenings } from '../../actions/happenings'
import { withStyles } from '@material-ui/core/styles'
import { styles } from './HappeningsList.styles'
import Button from '@material-ui/core/Button'
import { compose } from 'redux'

class HappeningsListContainer extends React.PureComponent {

  componentDidMount() {
    if(!this.props.happenings) this.props.getHappenings(0)
  }

  getMoreHappenings = () => {
    this.props.getHappenings(this.props.happenings.length)
  }
  render() {
    const { classes } = this.props
    return (
    <div className={classes.listContainer}>
        {this.props.happenings && <HappeningsList happenings={this.props.happenings} />}
        {this.props.hasMore && <Button size="large" color="secondary" position="right" variant="raised" style={{display: "block", marginLeft: "auto", marginRight: "3em"}} onClick={this.getMoreHappenings}>Get more</Button>}
    </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { 
    happenings: state.happenings.happenings,
    hasMore: state.happenings.hasMore
  }
}

export default compose(withStyles(styles), connect(mapStateToProps, { getHappenings }))(HappeningsListContainer)