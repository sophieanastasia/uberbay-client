import React, {PureComponent} from 'react'
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'

class AddHappeningForm extends PureComponent {
	state = {
    startDate: "2020-10-23T10:30",
    endDate: "2020-10-24T14:30"
  }

	handleSubmit = (e) => {
		e.preventDefault()
		this.props.handleSubmit(this.state)
	}

	handleChange = (event) => {
    const {name, value} = event.target

    this.setState({
      [name]: value
    })
  }

	render() {
    const { classes } = this.props
		return (
      <Paper className={classes.container} style={{marginTop: '80px'}} >
  			<form onSubmit={this.handleSubmit}>
          <Typography gutterBottom={true} variant="title">Make it happen</Typography>
          <TextField 
            className={classes.textField}
            type="text" 
            name="name" 
            required
            value={this.state.name || ''} 
            onChange={ this.handleChange } 
            label="Event name"
          />

          <TextField 
            className={classes.textField}
            type="datetime-local"
            name="startDate" 
            required
            value={this.state.startDate || ""}
            onChange={ this.handleChange } 
            label="Start date"
          />

          <TextField 
            className={classes.textField}
            type="datetime-local"
            name="endDate" 
            value={this.state.endDate || ""} 
            onChange={ this.handleChange } 
            label="End date"
          />

          <TextField 
            className={classes.textField}
            type="text" 
            name="image" 
            value={this.state.image || ''} 
            onChange={ this.handleChange } 
            label="Photo URL"
          />

          <TextField 
            className={classes.textField}
            type="text" 
            name="location"
            required 
            value={this.state.location || ''} 
            onChange={ this.handleChange } 
            label="Event Location"
          />

          <TextField 
            className={classes.textField}
            type="text" 
            name="description" 
            multiline
            required
            value={this.state.description || ""} 
            onChange={ this.handleChange }
            label="Description"
          />
  				<Button type="submit">Add Happening</Button>
  			</form>
      </Paper>
		)
	}
}

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    flexDirection: 'column',
    height: '50vh',
    justifyContent: 'center'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
})

export default withStyles(styles)(AddHappeningForm)