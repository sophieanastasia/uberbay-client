export const styles = theme => ({
  root: {
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    justify: 'center',
    alignItems: 'center',
    gridGap: `${theme.spacing.unit * 3}px`,
    gridTemplateRows: 'auto',
    gridTemplateColumns: 'auto',
    marginTop: '64px',
  },
  card: {
    margin: '1em',
    listStyle: 'none'
  },
  media: {
    height: 0,
    paddingTop: '56.25%',
    backgroundColor: 'grey'
  },
  listContainer: {
    display: 'block',
    flexDirection: 'row',
    width: '100%',
    margin: '1em'
  }
});