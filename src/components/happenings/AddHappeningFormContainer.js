import * as React from 'react'
import { addHappening } from '../../actions/happenings'
import { connect } from 'react-redux'
import AddHappeningForm from './AddHappeningForm'
import {withRouter} from 'react-router'

class AddHappeningFormContainer extends React.PureComponent {
  handleSubmit = (state) => {
    this.props.addHappening(state)
      .then(response => {
        this.props.history.push(`/happenings/${response.id}/tickets`)
      })
      .catch(err => console.error(err))
  }

  render() {
    return (
      <div>
        <AddHappeningForm handleSubmit={this.handleSubmit} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { 
    tickets: state.tickets ? state.tickets.tickets : null,
    hasMore: state.tickets ? state.tickets.hasMore : null
  }
}

export default withRouter(
  connect(mapStateToProps, { addHappening })(AddHappeningFormContainer)
)