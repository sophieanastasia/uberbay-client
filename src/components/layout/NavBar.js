import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import {withRouter} from 'react-router'
import {userId} from '../../jwt'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import './NavBar.css'

const NavBar = (props) => {
  const { location, history, user } = props
  return (
    <AppBar position="absolute" color="default" style={{zIndex:10}}>
      <Toolbar style={{justifyContent: 'space-between'}}>
        <Link to={"/"} className="main-logo">
          <Typography variant="title" color="inherit">
            UberBay
          </Typography>
        </Link>
        {
          (!user && location.pathname.indexOf('login') < 0) &&
          <Button color="inherit" onClick={() => history.push('/login')}>Login</Button>
        }
        {
          location.pathname.indexOf('login') > 0 &&
          <Button color="inherit" onClick={() => history.push('/signup')}>Sign up</Button>
        }
        {
          user &&
          <Button color="inherit" onClick={() => history.push('/addhappening')}>Add a Happening</Button>
        }
        {
          user &&
          <Button color="inherit" onClick={() => history.push('/addticket')}>Sell your Ticket</Button>
        }
        {
          user &&
          <Button color="inherit" onClick={() => history.push('/logout')}>Log out</Button>
        }
      </Toolbar>
    </AppBar>
  )
}

const mapStateToProps = state => ({
  user: state.currentUser && userId(state.currentUser.jwt)
})

export default withRouter(
  connect(mapStateToProps)(NavBar)
)