import React, {PureComponent} from 'react'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'

class LoginForm extends PureComponent {
	state = {}

	handleSubmit = (e) => {
		e.preventDefault()
		this.props.onSubmit(this.state)
	}

	handleChange = (event) => {
    const {name, value} = event.target

    this.setState({
      [name]: value
    })
  }

	render() {
    const { classes } = this.props
		return (
      <Paper >
  			<form className={classes.container} onSubmit={this.handleSubmit}>
          <Typography variant="title" >Welcome back!</Typography>
            <TextField 
              className={classes.textField}
              type="email" 
              name="email" 
              value={this.state.email || ''} 
              onChange={ this.handleChange }
              label="Email"
            />

            <TextField 
              className={classes.textField}
              type="password" 
              name="password" 
              value={this.state.password || ''} onChange={ this.handleChange } 
              label="Password"
            />
  				<Button type="submit">Login</Button>
  			</form>
		  </Paper>)
	}
}

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    flexDirection: 'column',
    height: '50vh',
    justifyContent: 'center'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
})

export default withStyles(styles)(LoginForm)