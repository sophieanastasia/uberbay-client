import {localStorageJwtKey} from './constants'
import {USER_LOGIN_SUCCESS, USER_LOGOUT} from './actions/users'

const jwtData = jwt => {
  var base64Url = jwt.split('.')[1]
  var base64 = base64Url.replace('-', '+').replace('_', '/')
  return JSON.parse(window.atob(base64))
}

export const userId = jwt => jwtData(jwt).id

export const isExpired = jwt => jwtData(jwt).exp < (Date.now()/1000)

export const storeJwt = store => next => action => {
  try {
    if (action.type === USER_LOGIN_SUCCESS) {
      localStorage.setItem(localStorageJwtKey, action.payload.jwt)
    }
    if (action.type === USER_LOGOUT) {
      localStorage.removeItem(localStorageJwtKey)
    }
  }
  catch (e) {
    console.log(`Interaction with LocalStorage went wrong`, e)
  }

  next(action)
}