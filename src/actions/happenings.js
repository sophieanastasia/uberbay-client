import request from 'superagent'
import { baseUrl } from '../constants'
import {isExpired} from '../jwt'
import { logout } from './users'

export const GET_HAPPENINGS = 'GET_HAPPENINGS'
export const POST_HAPPENINGS_SUCCESS = 'POST_HAPPENINGS_SUCCESS'

export const getHappenings = (start) => (dispatch) => {
  request
    .get(`${baseUrl}/happenings?start=${start}`)
    .then(result => dispatch({
      type: 'GET_HAPPENINGS',
      payload: {
        ...result.body
      }
      
    }))
    .catch(err => console.error(err))
}

export const addHappening = (happening) => (dispatch, getState) => {
  const state = getState()
  const jwt = state.currentUser.jwt

  if (isExpired(jwt)) return dispatch(logout())

  return request
    .post(`${baseUrl}/happenings`)
    .set(`Authorization`, `Bearer ${jwt}`)
    .send({
      ...happening,
      timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    })
    .then(res => {
      return res.body
    })
    .catch(err => console.error(err))
}