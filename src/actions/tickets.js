import request from 'superagent'
import { baseUrl } from '../constants'
import { isExpired, userId } from '../jwt'
import { logout } from './users'

export const GET_TICKETS = 'GET_TICKETS'
export const EDIT_TICKET = 'EDIT_TICKETS'
export const GET_CURRENT_TICKET = 'GET_CURRENT_TICKET'
export const GET_COMMENTS = 'GET_COMMENTS'

export const getTickets = (happeningId, start) => (dispatch) => {
  request
    .get(`${baseUrl}/happenings/${happeningId}/tickets?start=${start}`)
    .then(result => {
      dispatch({
      type: GET_TICKETS,
      payload: {
        ...result.body
      }
    })
  })

    .catch(err => console.error(err))
}

export const getCurrentTicket = (ticketId) => (dispatch) => {
  request
    .get(`${baseUrl}/tickets/${ticketId}`)
    .then(result => {
      return dispatch({
        type: GET_CURRENT_TICKET,
        payload: {
          ...result.body
        }
      })
    })
    .then(_ => dispatch(getComments(ticketId)))
}

export const buyTicket = (ticketId) => (dispatch, getState) => {
  const state = getState()
  const jwt = state.currentUser.jwt

  if (isExpired(jwt)) return dispatch(logout())

  request
    .patch(`${baseUrl}/tickets/${ticketId}/buy`)
    .set('Authorization', `Bearer ${jwt}`)
    .send({status: 'sold'})
    .then(result => dispatch({
      type: EDIT_TICKET,
      payload: result.body
    }))
    .catch(err => console.error(err))
}

export const addTicket = (ticket) => (dispatch, getState) => {
  const state = getState()
  const jwt = state.currentUser.jwt

  if (isExpired(jwt)) return dispatch(logout())

  return request
    .post(`${baseUrl}/tickets`)
    .set(`Authorization`, `Bearer ${jwt}`)
    .send({
      ...ticket,
      postTimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    })
    .then(res => res.body)
    .catch(err => console.error(err))
}

export const editTicket = (ticketId, changes) => (dispatch, getState) => {
  const state = getState()
  const jwt = state.currentUser.jwt

  if (isExpired(jwt)) return dispatch(logout())

  request
    .patch(`${baseUrl}/tickets/${ticketId}`)
    .set(`Authorization`, `Bearer ${jwt}`)
    .send({
      ...changes
    })
    .then(result => dispatch({
      type: EDIT_TICKET,
      payload: result.body
    }))
    .catch(err => console.error(err))
}

//COMMENTS

export const addComment = (ticketId, comment) => (dispatch, getState) => {
  const state = getState()
  const jwt = state.currentUser.jwt

  if (isExpired(jwt)) return dispatch(logout())
  request
    .post(`${baseUrl}/tickets/${ticketId}/comments`)
    .set('Authorization', `Bearer ${jwt}`)
    .send({
      ...comment,
      postTimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    })
    .then(_ => dispatch(getComments(ticketId)))
    .catch(err => console.error(err))
}

export const getComments = (ticketId) => (dispatch) => {
  request
    .get(`${baseUrl}/tickets/${ticketId}/comments`)
    .then(result => dispatch({
      type: GET_COMMENTS,
      payload: {
        ...result.body
      }
    }))
}