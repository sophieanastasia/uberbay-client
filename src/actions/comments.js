import request from 'superagent'
import { baseUrl } from '../constants'
import {isExpired} from '../jwt'
import { logout } from './users'

export const GET_COMMENTS = 'GET_COMMENTS'

export const addComment = (ticketId, comment) => (dispatch, getState) => {
  const state = getState()
  const jwt = state.currentUser.jwt

  if (isExpired(jwt)) return dispatch(logout())
  request
    .post(`${baseUrl}/tickets/${ticketId}/comments`)
    .set('Authorization', `Bearer ${jwt}`)
    .send({
      ...comment,
      postTimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
    })
    .then(result => getComments(ticketId))
    .catch(err => console.error(err))
}

export const getComments = (ticketId) => (dispatch) => {
  request
    .get(`${baseUrl}/tickets/${ticketId}/comments`)
    .then(result => dispatch({
      type: GET_COMMENTS,
      payload: [...result.body]
    }))
}